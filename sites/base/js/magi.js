/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//This javascript reads in the title of the slideshow and the images and caption of each slide to the project from a JSON file in the same directory. 

var jsonFile;
var imgCount = 0;
var count = 0;
var timer;
var ttype = [];
var tcont = [];
var ffam = [];
var fsty = [];
var fsize = [];
var imgArray = [];
var imgcArray = [];
var sliArray = [];
var slicArray = [];
var vidArray = [];
var vidnArray = [];
var vidcArray = [];
var ftfam = [];
var ftsty = [];
var ftsize = [];
var alevia;

function writeHere(){
jsonFile = 'site.json';

htmlFile = document.location.toString().split('/');
htmlFile = decodeURIComponent(htmlFile[htmlFile.length-1]);
var page;
  switch(htmlFile) {
    case "index.html":
      page = 0;
      break;
    case "posts.html":
      page = 1;
      break;
    case "gallery.html":
      page = 2;
      break;
    case "slideshow.html":
      page = 3;
      break;
    case "video.html":
      page = 4;
      break;
    default:
      page = 0;
  }

$.getJSON(jsonFile, function(data){
    var dom = data.pages[page];

    $.each(dom.texts, function(index, texts){
	ttype.push(texts.text_type);
        tcont.push(texts.text_content);
        ffam.push(texts.font_family);
        fsty.push(texts.font_style);
        fsize.push(texts.font_size);        
    });
    
    $.each(dom.images, function(index, images){
	imgArray.push(images.image_file_name);
	imgcArray.push(images.caption);
    });
 
    $.each(dom.slides, function(index, slides){
	sliArray.push(slides.image_file_name);
	slicArray.push(slides.caption);
    });

    $.each(dom.videos, function(index, videos){
	vidArray.push(videos.video_url);
        vidnArray.push(videos.video_name);
        vidcArray.push(videos.caption);
    });
    
    $.each(dom.footer_font, function(index, footer_font){
	ftfam.push(footer_font.font_family);
        ftsty.push(footer_font.font_style);
        ftsize.push(footer_font.font_size);
    });
//set the layout of the page

//set the color of the page
    
//set the page title
    document.getElementsByTagName('title').innerHTML = dom.page_title;

//set the banner text to the page title
    document.getElementById('title').innerHTML = dom.header;

//set the header of the banner page as student's name
    document.getElementById('student').innerHTML = data.student_name;

//set the logo in the banner area
    var logo = document.createElement("IMG");
    logo.setAttribute("src", "./img/" + dom.banner_img);
    logo.setAttribute("align", "center");
    document.getElementById("logo").appendChild(logo);    

//Shows the text posts
    var b;
    var n;
    var t;
    for(t = 0; t < ttype.length; t++){
        switch(ttype[t]) {
            case "Heading1":
                var b = document.createElement("H1");
                break;
            case "Heading2":
                var b = document.createElement("H2");
                break;                
            case "Heading3":
                var b = document.createElement("H3");
                break;
            case "Paragraph":
                var b = document.createElement("P");
                break;
            case "List":
                var b = document.createElement("LI");
                break;
            default:
                var b = document.createElement("P");
                break;
        }
        var n = document.createTextNode(tcont[t]);
        b.appendChild(n);
        b.setAttribute("id", "post"+t);
        b.style.fontFamily = ffam[t];
        b.style.fontStyle = fsty[t];
        b.style.fontSize = fsize[t];
        document.getElementById("posts").appendChild(b);
    }
    
//Shows the images
    var m;
    for(m = 0; m < imgArray.length; m++){
        var k = document.createElement("IMG");
        k.setAttribute("id", "image"+m);
        k.setAttribute("src", "./img/" + imgArray[m]);
        k.setAttribute("width", 240);
        k.setAttribute("height", 180);
        k.setAttribute("alt", imgcArray[m]);
        k.style.cssFloat = "left";
        document.getElementById("imgs").appendChild(k);
    }
    
//set the header of the slideshow page as Slideshow's title
    document.getElementById('slidetitle').innerHTML = dom.sstitle;

//Shows the first slide in the slideshow    
    var x = document.createElement("IMG");
    x.setAttribute("id", "alevia");
    x.setAttribute("src", "./img/" + sliArray[0]);
    x.setAttribute("align", "center");
    x.setAttribute("alt", slicArray[0]);
    document.getElementById("slideimg").appendChild(x);
    document.getElementById("caption").innerHTML = (imgcArray[0]);
//Goes to next slide after some time, acts as an autoplay feature
    timer = setInterval("nextImage()", 2200);

//set the header of the video page as Video's name
    document.getElementById('vidtitle').innerHTML = vidnArray[0];

//Shows the video uploaded or embedded by the user
    var v = document.createElement("IFRAME");
    v.setAttribute("src", vidArray[0]);
    v.setAttribute("width", 640);
    v.setAttribute("height", 480);
    v.setAttribute("alt", vidcArray[0]);
    document.getElementById("vid").appendChild(v);

//Shows the text for the footer    
    document.getElementById('footer').innerHTML = dom.footer;
    document.getElementById('footer').style.fontFamily = ftfam[0];
    document.getElementById('footer').style.fontStyle = ftsty[0];
    document.getElementById('footer').style.fontSize = ftsize[0];

});
}


function nextImage() {
imgCount = imgCount + 1;

if(imgCount < 0) imgCount = sliArray.length-1;
if(imgCount > sliArray.length-1) imgCount = 0;
    var x = document.createElement("IMG");
    x.setAttribute("id", "alevia");
    x.setAttribute("src", "./img/" + sliArray[imgCount]);
    x.setAttribute("align", "center");
    x.setAttribute("alt", slicArray[imgCount]);
    document.getElementById("slideimg").appendChild(x);
    document.getElementById("caption").innerHTML = (slicArray[imgCount]);

alevia = document.getElementById("alevia");
alevia.parentNode.removeChild(x.previousSibling);

count = count +1;
}


function prevImage() {
imgCount = imgCount - 1;

if(imgCount < 0) imgCount = sliArray.length-1;
if(imgCount > sliArray.length-1) imgCount=0;
    var x = document.createElement("IMG");
    x.setAttribute("id", "alevia");
    x.setAttribute("src", "./img/" + sliArray[imgCount]);
    x.setAttribute("align", "center");
    x.setAttribute("alt", slicArray[imgCount]);
    document.getElementById("slideimg").appendChild(x);
    document.getElementById("caption").innerHTML = (slicArray[imgCount]);

alevia = document.getElementById("alevia");
alevia.parentNode.removeChild(x.previousSibling);

count = count +1;
}

function play(){
timer = setInterval("nextImage()", 2200);
$('#pause').show();
$('#play').hide();
}

function pause(){
clearInterval(timer);
$('#pause').hide();
$('#play').show();
}
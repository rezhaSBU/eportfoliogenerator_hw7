package pg.controller;

import static pg.LangProp.DEFAULT_COMPONENT_TYPE;
import static pg.LangProp.DEFAULT_FONT_FAMILY;
import static pg.LangProp.DEFAULT_FONT_SIZE;
import static pg.LangProp.DEFAULT_FONT_STYLE;
import properties_manager.PropertiesManager;
import static pg.LangProp.DEFAULT_IMAGE_CAPTION;
import static pg.LangProp.DEFAULT_IMAGE_NAME;
import static pg.LangProp.DEFAULT_IMAGE_PATH;
import static pg.LangProp.DEFAULT_TEXT_CONTENT;
import static pg.LangProp.DEFAULT_VIDEO_HEIGHT;
import static pg.LangProp.DEFAULT_VIDEO_NAME;
import static pg.LangProp.DEFAULT_VIDEO_PATH;
import static pg.LangProp.DEFAULT_VIDEO_WIDTH;
import static pg.StartConts.DEFAULT_PAGE_IMAGE;
import static pg.StartConts.PATH_PORT_IMAGES;
import pg.model.Page;
import pg.view.PortGenView;

/**
 * This controller provides responses for the page edit toolbar,
 * which allows the user to add, remove, and reorder components.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PageEditController {
    // APP UI
    private PortGenView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PageEditController(PortGenView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddTextRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addTextComponent(props.getProperty(DEFAULT_COMPONENT_TYPE), 
                props.getProperty(DEFAULT_TEXT_CONTENT), props.getProperty(DEFAULT_FONT_FAMILY), 
                props.getProperty(DEFAULT_FONT_STYLE), props.getProperty(DEFAULT_FONT_SIZE));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveTextRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	page.removeSelectedComponent();
	ui.reloadPortfolioPane();
    }

   /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddImageRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addImageComponent(props.getProperty(DEFAULT_COMPONENT_TYPE), props.getProperty(DEFAULT_IMAGE_NAME), props.getProperty(DEFAULT_IMAGE_PATH), props.getProperty(DEFAULT_IMAGE_CAPTION));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveImageRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	page.removeSelectedComponent();
	ui.reloadPortfolioPane();
    }

       /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddVideoRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addVideoComponent(props.getProperty(DEFAULT_COMPONENT_TYPE), props.getProperty(DEFAULT_VIDEO_NAME), props.getProperty(DEFAULT_VIDEO_PATH), Integer.valueOf(props.getProperty(DEFAULT_VIDEO_WIDTH)), Integer.valueOf(props.getProperty(DEFAULT_VIDEO_HEIGHT)));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveVideoRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	page.removeSelectedComponent();
	ui.reloadPortfolioPane();
    }
    
   /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
//    public void processAddSlideshowRequest() {
//	Page page = ui.getPortfolio().getSelectedPage();
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
//	page.addComponent(DEFAULT_PAGE_IMAGE, PATH_PORT_IMAGES, props.getProperty(DEFAULT_IMAGE_CAPTION));
//    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveSlideshowRequest() {
	Page page = ui.getPortfolio().getSelectedPage();
	page.removeSelectedComponent();
	ui.reloadPortfolioPane();
    }
}

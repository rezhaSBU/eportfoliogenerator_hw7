package pg.controller;

import properties_manager.PropertiesManager;
import static pg.LangProp.DEFAULT_IMAGE_CAPTION;
import static pg.LangProp.DEFAULT_PAGE_COLOR;
import static pg.LangProp.DEFAULT_PAGE_LAYOUT;
import static pg.LangProp.DEFAULT_PAGE_TITLE;
import static pg.StartConts.DEFAULT_PAGE_IMAGE;
import static pg.StartConts.PATH_PORT_IMAGES;
import pg.model.PortModel;
import pg.view.PortGenView;

/**
 * This controller provides responses for the portfolio edit toolbar,
 * which allows the user to add, remove, and reorder pages.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PortEditController {
    // APP UI
    private PortGenView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PortEditController(PortGenView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * page to the portfolio.
     */
    public void processAddPageRequest() {
	PortModel portfolio = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	portfolio.addPage(props.getProperty(DEFAULT_PAGE_TITLE));
	ui.reloadPortfolioPane();        
    }

    /**
     * Provides a response for when the user has selected a page
     * and wishes to remove it from the portfolio.
     */
    public void processRemovePageRequest() {
	PortModel portfolio = ui.getPortfolio();
	portfolio.removeSelectedPage();
	ui.reloadPortfolioPane();
    }

    /**
     * Provides a response for when the user has selected a page
     * and wishes to move it up in the portfolio.
     */
    public void processMovePageUpRequest() {
	PortModel portfolio = ui.getPortfolio();
	portfolio.moveSelectedPageUp();	
	ui.reloadPortfolioPane();	
    }

    /**
     * Provides a response for when the user has selected a page
     * and wises to move it down in the portfolio.
     */
    public void processMovePageDownRequest() {
	PortModel portfolio = ui.getPortfolio();
	portfolio.moveSelectedPageDown();	
	ui.reloadPortfolioPane();
    }
    
    public void processUpdateLayoutRequest(){
	PortModel portfolio = ui.getPortfolio();
	portfolio.updateLayout();
	ui.reloadPortfolioPane();        
    }
    public void processUpdateColorRequest(){
	PortModel portfolio = ui.getPortfolio();
	portfolio.updateColor();
	ui.reloadPortfolioPane();        
    }
    public void processUpdateFontRequest(){
	PortModel portfolio = ui.getPortfolio();
	portfolio.updateFont();
	ui.reloadPortfolioPane();        
    }
    public void processUpdateTextRequest(){
	PortModel portfolio = ui.getPortfolio();
	portfolio.updateText();
	ui.reloadPortfolioPane();        
    }
    public void processUpdateImageRequest(){
	PortModel portfolio = ui.getPortfolio();
	portfolio.updateImage();
	ui.reloadPortfolioPane();        
    }
//    public void processUpdateSlideRequest(){
//	PortModel portfolio = ui.getPortfolio();
//	portfolio.updateSlide();
//	ui.reloadPortfolioPane();        
//    }
    public void processUpdateVideoRequest(){
	PortModel portfolio = ui.getPortfolio();
	portfolio.updateVideo();
	ui.reloadPortfolioPane();        
    }
}

package pg;

/**
 * This class provides the types that will be need to be loaded from
 * language-dependent XML files.
 * 
 * @author McKilla Gorilla & _____________
 */
public enum LangProp {
    // TITLE FOR WINDOW TITLE BAR
    // TITLE FOR WINDOW TITLE BAR
    TITLE_WINDOW,
    
    // APPLICATION TOOLTIPS FOR BUTTONS
    TOOLTIP_NEW_PORT,
    TOOLTIP_LOAD_PORT,
    TOOLTIP_SAVE_PORT,
    TOOLTIP_SAVE_AS_PORT,
    TOOLTIP_EXPORT_PORT,
    TOOLTIP_EXIT,
    TOOLTIP_ADD_PAGE,
    TOOLTIP_REMOVE_PAGE,
    TOOLTIP_MOVE_UP,
    TOOLTIP_MOVE_DOWN,
    TOOLTIP_PREVIOUS_PAGE,
    TOOLTIP_NEXT_PAGE,
    TOOLTIP_CHOOSE_COLOR,
    TOOLTIP_CHOOSE_LAYOUT,
    TOOLTIP_CHOOSE_FONT,
    TOOLTIP_ADD_TEXT,
    TOOLTIP_REMOVE_TEXT,
    TOOLTIP_ADD_IMAGE,
    TOOLTIP_REMOVE_IMAGE,
    TOOLTIP_ADD_VIDEO,
    TOOLTIP_REMOVE_VIDEO,
    TOOLTIP_ADD_SLIDESHOW,
    TOOLTIP_REMOVE_SLIDESHOW,    
    
    // DEFAULT VALUES
    DEFAULT_COMPONENT_TYPE,
    DEFAULT_TEXT_CONTENT,
    DEFAULT_FONT_FAMILY,
    DEFAULT_FONT_STYLE,
    DEFAULT_FONT_SIZE,
    DEFAULT_VIDEO_NAME,
    DEFAULT_VIDEO_PATH,
    DEFAULT_VIDEO_WIDTH,
    DEFAULT_VIDEO_HEIGHT,
    DEFAULT_IMAGE_NAME,
    DEFAULT_IMAGE_PATH,
    DEFAULT_IMAGE_CAPTION,
    DEFAULT_PORT_TITLE,
    DEFAULT_PAGE_TITLE,
    DEFAULT_PAGE_LAYOUT,
    DEFAULT_PAGE_COLOR,
    
    // UI LABELS
    LABEL_CAPTION,
    LABEL_PORT_TITLE,
      
    /* THESE ARE FOR LANGUAGE-DEPENDENT ERROR HANDLING,
     LIKE FOR TEXT PUT INTO DIALOG BOXES TO NOTIFY
     THE USER WHEN AN ERROR HAS OCCURED */
    ERROR_DATA_FILE_LOADING,
    ERROR_PROPERTIES_FILE_LOADING,
    ERROR_NO_PORT_IMAGES,
    ERROR_UNEXPECTED
}

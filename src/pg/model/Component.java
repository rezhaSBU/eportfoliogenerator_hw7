package pg.model;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & _____________
 */
public class Component extends VBox{
    Label componentLabel = new Label(); //header, paragraph, list, image, slideshow, video
    String type; //header, paragraph, list, image, slideshow, video
    String imageFileName;
    String imagePath;
    String imageFloat;
    String headerContent;
    String headerFontFam;
    String headerFontSty;
    String headerFontSi;
    String paragraphContent;
    String paragraphFontFam;
    String paragraphFontSty;
    String paragraphFontSi;
    String listContent;
    String listFontFam;
    String listFontSty;
    String listFontSi;
    String footerContent;
    String footerFontFam;
    String footerFontSty;
    String footerFontSi;    
    String sstitle;    
    String videoFileName;
    String videoPath;
    int videoWidth;
    int videoHeight;

    
    /**
     * Constructor, creates an Text component.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     * @param initCaption Textual caption to appear under the image.
     */
    public Component(String initType, String initContent, String initFontFam, String initFontSty, String initFontSi) {
	type = initType;
        switch (type) {
                case "Header":
                    headerContent = initContent;
                    headerFontFam = initFontFam;
                    headerFontSty = initFontSty;
                    headerFontSi = initFontSi;
                    break;
                case "Paragraph":
                    paragraphContent = initContent;
                    paragraphFontFam = initFontFam;
                    paragraphFontSty = initFontSty;
                    paragraphFontSi = initFontSi;
                    break; 
                case "List":
                    listContent = initContent;
                    listFontFam = initFontFam;
                    listFontSty = initFontSty;
                    listFontSi = initFontSi;                    
                    break;
                case "Footer":
                    footerContent = initContent;
                    footerFontFam = initFontFam;
                    footerFontSty = initFontSty;
                    footerFontSi = initFontSi;                    
                    break;                    
            }
    }
    
    /**
     * Constructor, creates an Image component.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     * @param initCaption Textual caption to appear under the image.
     */
    public Component(String initType, String initImageFileName, String initImagePath, String initImageFloat) {
	type = initType;
        imageFileName = initImageFileName;
	imagePath = initImagePath;
	imageFloat = initImageFloat;
    }

     /**
     * Constructor, creates an Video component.
     * @param initVideoFileName File name of the video.
     * 
     * @param initVideoPath File path for the video.
     * 
     * @param initVideoHeight height of video.
     *
     * @param initVideoWidth width of video.
     */
    public Component(String initType, String initVideoFileName, String initVideoPath, int initVideoHeight, int initVideoWidth) {
	type = initType;
        videoFileName = initVideoFileName;
	videoPath = initVideoPath;
	videoHeight = initVideoWidth;
	videoWidth = initVideoWidth;
    }
        
////////////////////////////////////////////////////////////////////
    public Component(String initType) {
        type = initType;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getImageFloat() { return imageFloat; }
//////////////////////////////////////////////////////////////////////    
    public String getType() { return type; }
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImageFloat(String initImageFloat) {
	imageFloat = initImageFloat;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
///////////////////////////////////////////////////////////////////    
    public void setType(String initType) {
	type = initType;
    }
    
}

package pg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import pg.LangProp;
import pg.view.ColorSelectionDialog;
import pg.view.FontSelectionDialog;
import pg.view.ImageDialog;
import pg.view.LayoutSelectionDialog;
import pg.view.PortGenView;
import pg.view.TextDialog;
import pg.view.VideoDialog;

/**
 * This class manages all the data associated with a portfolio.
 * 
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class PortModel {
    LayoutSelectionDialog layoutDialog;
    ColorSelectionDialog colorDialog;
    PortGenView ui;
    String title;
    ObservableList<Page> pages;
    Page selectedPage;
    String fontFam = "Nunita 700";
    String fontSty = "normal";
    String fontSi = "1.5em";

    public PortModel(PortGenView initUI) {
	ui = initUI;
	pages = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isPageSelected() {
	return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage) {
	return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
    public Page getSelectedPage() {
	return selectedPage;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedPage(Page initSelectedPage) {
	selectedPage = initSelectedPage;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the portfolio to have no pages and a default title.
     */
    public void reset() {
	pages.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LangProp.DEFAULT_PORT_TITLE);
	selectedPage = null;
    }

    /**
     * Adds a page to the portfolio with the parameter settings.
     * @param initImageFileName File name of the page image to add.
     * @param initImagePath File path for the page image to add.
     * @param initCaption Caption for the page image to add.
     */
//    public void addPage(   String initImageFileName,
//			    String initImagePath,
//			    String initCaption) {
//	Page pageToAdd = new Page(initImageFileName, initImagePath, initCaption);
//	pages.add(pageToAdd);
//	ui.reloadPortfolioPane();
//        ui.updateToolbarControls(false);
//    }

//////////////////////////////////////////////////////////////////    
    public void addPage(Page initPage) {
	Page pageToAdd = initPage;
	pages.add(pageToAdd);
	ui.reloadPortfolioPane();
        ui.updateToolbarControls(false);
    }

    public void addPage(String initPageTitle) {
	Page pageToAdd = new Page(initPageTitle);
	pages.add(pageToAdd);
	ui.reloadPortfolioPane();
        ui.updateToolbarControls(false);
    }
    /**
     * Removes the currently selected page from the portfolio and
     * updates the display.
     */
    public void removeSelectedPage() {
	if (isPageSelected()) {
	    pages.remove(selectedPage);
	    selectedPage = null;
	    ui.reloadPortfolioPane();
            ui.updateToolbarControls(false);
	}
    }
 
    /**
     * Moves the currently selected page up in the page
     * show by one page.
     */
    public void moveSelectedPageUp() {
	if (isPageSelected()) {
	    movePageUp(selectedPage);
	    ui.reloadPortfolioPane();
            ui.updateToolbarControls(false);
	}
    }
    
    // HELPER METHOD
    private void movePageUp(Page pageToMove) {
	int index = pages.indexOf(pageToMove);
	if (index > 0) {
	    Page temp = pages.get(index);
	    pages.set(index, pages.get(index-1));
	    pages.set(index-1, temp);
	}
    }
    
    /**
     * Moves the currently selected page down in the page
     * show by one page.
     */
    public void moveSelectedPageDown() {
	if (isPageSelected()) {
	    movePageDown(selectedPage);
	    ui.reloadPortfolioPane();
            ui.updateToolbarControls(false);
	}
    }
    
    // HELPER METHOD
    private void movePageDown(Page pageToMove) {
	int index = pages.indexOf(pageToMove);
	if (index < (pages.size()-1)) {
	    Page temp = pages.get(index);
	    pages.set(index, pages.get(index+1));
	    pages.set(index+1, temp);
	}
    }
    
    /**
     * Changes the currently selected page to the previous page
     * in the portfolio.
     */
    public void previous() {
	if (selectedPage == null)
	    return;
	else {
	    int index = pages.indexOf(selectedPage);
	    index--;
	    if (index < 0)
		index = pages.size() - 1;
	    selectedPage = pages.get(index);
	}
    }

    /**
     * Changes the currently selected page to the next page
     * in the portfolio.
     */    
    public void next() {
    	if (selectedPage == null)
	    return;
	else {
	    int index = pages.indexOf(selectedPage);
	    index++;
	    if (index >= pages.size())
		index = 0;
	    selectedPage = pages.get(index);
	}
    }    
    
    public void updateLayout() {
	if (isPageSelected()) {
            layoutDialog = new LayoutSelectionDialog();
            layoutDialog.showAndWait();
            selectedPage.setPageLayout(layoutDialog.getSelectedLayout());
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
        
    public void updateColor() {
	if (isPageSelected()) {
	    ColorSelectionDialog colorDialog = new ColorSelectionDialog();
            colorDialog.showAndWait();
            selectedPage.setPageColor(colorDialog.getSelectedColor());     
            selectedPage.addComponent(title);
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
        
    public void updateFont() {
	if (isPageSelected()) {
	    FontSelectionDialog fontDialog = new FontSelectionDialog();
            fontDialog.showAndWait();
            selectedPage.setFontFam(fontDialog.getSelectedFont());   
            selectedPage.addComponent(title);
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
        
    public void updateText() {
	if (isPageSelected()) {
	    TextDialog textDialog = new TextDialog();
            textDialog.showAndWait();
            selectedPage.addTextComponent(textDialog.getSelectedType(), textDialog.getContent(), 
                    fontFam, fontSty, fontSi);
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
        
    public void updateImage() {
	if (isPageSelected()) {
	    ImageDialog imageDialog = new ImageDialog();
            imageDialog.showAndWait();
            selectedPage.addImageComponent("image", imageDialog.getSelectedImageName(), 
                    imageDialog.getSelectedImagePath(), imageDialog.getSelectedImageFloat());
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
        
//    public void updateSlide() {
//	if (isPageSelected()) {
//	    SlideshowDialog slideDialog = new SlideshowDialog();
//            slideDialog.showAndWait();
//            selectedPage.addSlideComponent(title);
//	    ui.reloadPagePane();
//            ui.updateToolbarControls(false);
//	}
//    }
        
    public void updateVideo() {
	if (isPageSelected()) {
	    VideoDialog videoDialog = new VideoDialog();
            videoDialog.showAndWait();
            selectedPage.addVideoComponent("video", videoDialog.getSelectedVideoName(), 
                    videoDialog.getSelectedVideoPath(), videoDialog.getSelectedVideoWidth(), 
                    videoDialog.getSelectedVideoHeight());
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
}
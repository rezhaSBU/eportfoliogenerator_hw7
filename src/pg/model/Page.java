package pg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import pg.LangProp;
import pg.view.PortGenView;
import properties_manager.PropertiesManager;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class Page extends VBox{
    String pageTitle;
    String layout;
    String color;
    String fontFamily;
    String fontStyle;
    String fontSize;
    String bannerFileName;
    String bannerPath;
    PortGenView ui;
    ObservableList<Component> components;
    Component selectedComponent;
    
    /**
     * Constructor, it initializes all slide data.
     * @param initPageTitle Title and url of the page
     * 
     * @param initLayout File name of the layout css
     * 
     * @param initcolor File name of the color css
     * 
     * @param initBannerFileName File name of the image.
     * 
     * @param initBannerPath File path for the image.
     */

    public Page(String initPageTitle) {
        pageTitle = initPageTitle;
    }
    
    public Page(String initPageTitle, String initLayout, String initColor) {
	pageTitle = initPageTitle;
	layout = initLayout;
	color = initColor;
    }

    public Page(String initPageTitle, String initLayout, String initColor, 
            String initBannerFileName, String initBannerPath) {
	pageTitle = initPageTitle;
	layout = initLayout;
	color = initColor;
        bannerFileName = initBannerFileName;
        bannerPath = initBannerPath;
    }
 
    
    public Page(PortGenView initUI) {
	ui = initUI;
	components = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isComponentSelected() {
	return selectedComponent != null;
    }
    
    public boolean isSelectedComponent(Component testComponent) {
	return selectedComponent == testComponent;
    }
    
    public ObservableList<Component> getComponents() {
	return components;
    }
    
    public Component getSelectedComponents() {
	return selectedComponent;
    }

    
    // MUTATOR METHODS
    public void setSelectedComponent(Component initSelectedComponent) {
	selectedComponent = initSelectedComponent;
    }

    // SERVICE METHODS
    
    /**
     * Resets the portfolio to have no pages and a default title.
     */
    public void reset() {
	components.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	pageTitle = props.getProperty(LangProp.DEFAULT_PORT_TITLE);
	selectedComponent = null;
    }

    /**
     * Adds a page to the portfolio with the parameter settings.
     * @param initImageFileName File name of the page image to add.
     * @param initImagePath File path for the page image to add.
     * @param initCaption Caption for the page image to add.
     */
    public void addComponent( Component initComponent) {
	Component componentToAdd = initComponent;
	components.add(componentToAdd);
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }
    
    public void addComponent( String initType) {
	Component componentToAdd = new Component(initType);
        componentToAdd.componentLabel = new Label(initType);        
	components.add(componentToAdd);
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }
    
    public void addLayoutComponent( String initType, String initLayout) {
	Component componentToAdd = new Component(initType);
        componentToAdd.componentLabel = new Label(initLayout);
	components.add(componentToAdd);        
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }
        
    public void addColorComponent( String initType, String initColor) {
	Component componentToAdd = new Component(initType);
        componentToAdd.componentLabel = new Label(initColor);
        components.add(componentToAdd);
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }

    public void addTextComponent(String initType, String initContent, String initFontFam, String initFontSty, String initFontSi) {
        Component componentToAdd = new Component(initType, initContent, initFontFam, initFontSty, initFontSi);
	switch (initType) {
                case "Header":
                    componentToAdd.headerContent = initContent;
                    componentToAdd.headerFontFam = initFontFam;
                    componentToAdd.headerFontSty = initFontSty;
                    componentToAdd.headerFontSi = initFontSi;
                    break;
                case "Paragraph":
                    componentToAdd.paragraphContent = initContent;
                    componentToAdd.paragraphFontFam = initFontFam;
                    componentToAdd.paragraphFontSty = initFontSty;
                    componentToAdd.paragraphFontSi = initFontSi;
                    break; 
                case "List":
                    componentToAdd.listContent = initContent;
                    componentToAdd.listFontFam = initFontFam;
                    componentToAdd.listFontSty = initFontSty;
                    componentToAdd.listFontSi = initFontSi;                    
                    break;
                case "Footer":
                    componentToAdd.footerContent = initContent;
                    componentToAdd.footerFontFam = initFontFam;
                    componentToAdd.footerFontSty = initFontSty;
                    componentToAdd.footerFontSi = initFontSi;                    
                    break;                    
            }
        componentToAdd.componentLabel = new Label(initType);        
        components.add(componentToAdd);
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }
    
    /**
     * Constructor, creates an Image component.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     * @param initImageFloat Textual description of how to float the image.
     */
    public void addImageComponent(String initType, String initImageFileName, String initImagePath, String initImageFloat) {
	Component componentToAdd = new Component(initType, initImageFileName, initImagePath, initImageFloat);
        componentToAdd.imageFileName = initImageFileName;
        componentToAdd.imagePath = initImagePath;
        componentToAdd.imageFloat = initImageFloat;
	components.add(componentToAdd);
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }

     /**
     * Constructor, creates an Video component.
     * @param initVideoFileName File name of the video.
     * 
     * @param initVideoPath File path for the video.
     * 
     * @param initVideoHeight height of video.
     *
     * @param initVideoWidth width of video.
     */
    public void addVideoComponent(String initType, String initVideoFileName, String initVideoPath, int initVideoHeight, int initVideoWidth) {
	Component componentToAdd = new Component(initType, initVideoFileName, initVideoPath, initVideoWidth, initVideoHeight);
        componentToAdd.videoFileName = initVideoFileName;
        componentToAdd.videoPath = initVideoPath;
        componentToAdd.videoWidth = initVideoWidth;
        componentToAdd.videoHeight = initVideoHeight;
	components.add(componentToAdd);
	ui.reloadPagePane();
        ui.updateToolbarControls(false);
    }
        
////////////////////////////////////////////////////////////////////
    /**
     * Removes the currently selected page from the portfolio and
     * updates the display.
     */
    public void removeSelectedComponent() {
	if (isComponentSelected()) {
	    components.remove(selectedComponent);
	    selectedComponent = null;
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
 
    /**
     * Moves the currently selected page up in the page
     * show by one page.
     */
    public void moveSelectedComponentUp() {
	if (isComponentSelected()) {
	    moveComponentUp(selectedComponent);
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
    
    // HELPER METHOD
    private void moveComponentUp(Component componentToMove) {
	int index = components.indexOf(componentToMove);
	if (index > 0) {
	    Component temp = components.get(index);
	    components.set(index, components.get(index-1));
	    components.set(index-1, temp);
	}
    }
    
    /**
     * Moves the currently selected page down in the page
     * show by one page.
     */
    public void moveSelectedComponentDown() {
	if (isComponentSelected()) {
	    moveComponentDown(selectedComponent);
	    ui.reloadPagePane();
            ui.updateToolbarControls(false);
	}
    }
    
    // HELPER METHOD
    private void moveComponentDown(Component componentToMove) {
	int index = components.indexOf(componentToMove);
	if (index < (components.size()-1)) {
	    Component temp = components.get(index);
	    components.set(index, components.get(index+1));
	    components.set(index+1, temp);
	}
    }
    
    /**
     * Changes the currently selected page to the previous page
     * in the portfolio.
     */
    public void previous() {
	if (selectedComponent == null)
	    return;
	else {
	    int index = components.indexOf(selectedComponent);
	    index--;
	    if (index < 0)
		index = components.size() - 1;
	    selectedComponent = components.get(index);
	}
    }

    /**
     * Changes the currently selected page to the next page
     * in the portfolio.
     */    
    public void next() {
    	if (selectedComponent == null)
	    return;
	else {
	    int index = components.indexOf(selectedComponent);
	    index++;
	    if (index >= components.size())
		index = 0;
	    selectedComponent = components.get(index);
	}
    }
    
    // ACCESSOR METHODS  
    public String getPageLayout() { return layout; }
    public String getPageColor() { return color; }
    public String getTitle() { return pageTitle; }
    public String getBannerFileName() { return bannerFileName; }
    public String getBannerPath() { return bannerPath; }
    public String getFontFam() { return fontFamily; }
    public String getFontSty() { return fontStyle; }
    public String getFontSi() { return fontSize; }
    // MUTATOR METHODS
    public void setPageLayout(String initLayout) { 
	layout = initLayout; 
    }
    
    public void setPageColor(String initColor) { 
	color = initColor; 
    }
    
    public void setTitle(String initTitle) { 
	pageTitle = initTitle; 
    }
    
    public void setBanner(String initBannerFileName, String initBannerPath) { 
	bannerFileName = initBannerFileName; 
        bannerPath = initBannerPath; 
    }
    
    public void setBannerFileName(String initBannerFileName) { 
	bannerFileName = initBannerFileName; 
    }
    
    public void setBannerPath(String initBannerPath) { 
	bannerPath = initBannerPath; 
    }
    public void setFontFam(String initFontFam) { 
	fontFamily = initFontFam; 
    }
    public void setFontSty(String initFontSty) { 
	fontStyle = initFontSty; 
    }
    public void setFontSi(String initFontSi) { 
	fontSize = initFontSi; 
    }
}

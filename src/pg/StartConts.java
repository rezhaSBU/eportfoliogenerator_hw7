package pg;

import pg.*;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * 
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class StartConts {
    public static String ENGLISH_LANG = "English";
    public static String SPANISH_LANG = "Spanish";

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String UI_PROPERTIES_FILE_NAME_English = "properties_EN.xml";
    public static String UI_PROPERTIES_FILE_NAME_Spanish = "properties_SP.xml";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_PORTS = PATH_DATA + "slide_shows/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_PORT_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "pg/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "PortGenStyle.css";
    public static String PATH_BASE = "./sites/base/";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "SSM_Logo.png";
    public static String ICON_NEW_PORT = "New.png";
    public static String ICON_LOAD_PORT = "Load.png";
    public static String ICON_SAVE_PORT = "Save.png";
    public static String ICON_SAVE_AS_PORT = "Saveas.png";
    public static String ICON_VIEW_PORT = "View.png";
    public static String ICON_EXPORT_PORT = "Export.png";   
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "Add.png";
    public static String ICON_REMOVE_PAGE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_CHOOSE_COLOR = "Color.png";
    public static String ICON_CHOOSE_LAYOUT = "Layout.png";
    public static String ICON_LAYOUT1 = "layout1.png";
    public static String ICON_LAYOUT2 = "layout2.png";
    public static String ICON_LAYOUT3 = "layout3.png";
    public static String ICON_LAYOUT4 = "layout4.png";
    public static String ICON_LAYOUT5 = "layout5.png";
    public static String ICON_COLOR1 = "color1.png";
    public static String ICON_COLOR2 = "color2.png";
    public static String ICON_COLOR3 = "color3.png";
    public static String ICON_COLOR4 = "color4.png";
    public static String ICON_COLOR5 = "color5.png";
    public static String ICON_CHOOSE_FONT = "Font.png";
    public static String ICON_ADD_TEXT = "Text.png";
    public static String ICON_ADD_IMAGE = "Image.png";
    public static String ICON_ADD_VIDEO = "Video.png";
    public static String ICON_ADD_SLIDESHOW = "Slideshow.png";
    public static String ICON_REMOVE_TEXT = "removeText.png";
    public static String ICON_REMOVE_IMAGE = "removeImage.png";
    public static String ICON_REMOVE_VIDEO = "removeVideo.png";
    public static String ICON_REMOVE_SLIDESHOW = "removeSlideshow.png";    
    // UI SETTINGS
    public static String    DEFAULT_PAGE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_PORT_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_DIALOG_BOX = "dialog_box";
    public static String    CSS_CLASS_BOXES = "boxes";
    public static String    CSS_CLASS_BUTN = "butn";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_MASTER = "horizontal_toolbar_master";
    public static String    CSS_CLASS_PORT_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_PAGE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SELECTED_PAGE_EDIT_VIEW = "selected_slide_edit_view";
    
    // UI LABELS
    public static String    LABEL_PORT_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_SELECTION_PROMPT = "Select a Language:";
    public static String    OK_BUTTON_TEXT = "OK";
}

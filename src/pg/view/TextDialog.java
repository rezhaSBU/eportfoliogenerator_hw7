package pg.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class TextDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    VBox container;
    Label textPromptLabel;
    ComboBox textComboBox;
    Button okButton;
    String selectedType = "Choose a type";
    String content = "blank";
    ObservableList<String> listContent = FXCollections.observableArrayList();
    TextField headerTextField;
    TextArea paragraphTextField;
    Button listAddButton;
    TextField listTextField;
    TextField footerTextField;
    public TextDialog() {
	textPromptLabel = new Label("Select a text type");
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> textChoices = FXCollections.observableArrayList();
	textChoices.add("Header");
	textChoices.add("Paragraph");
	textChoices.add("List");
	textChoices.add("Footer");
        container = new VBox();
        container.setVisible(false);
        headerTextField = new TextField();
        paragraphTextField = new TextArea();
        listTextField = new TextField();
	listAddButton = new Button("Add list item");
        footerTextField = new TextField();
        textComboBox = new ComboBox(textChoices);
        textComboBox.getSelectionModel().select("Header");
	okButton = new Button(OK_BUTTON_TEXT);
	
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(textPromptLabel);
	vBox.getChildren().add(textComboBox);
        vBox.getChildren().add(container);
	gBox.add(vBox, 0, 0, 2, 1);
        gBox.add(okButton, 1, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
        
        textComboBox.setOnAction(e -> {
            selectedType = textComboBox.getSelectionModel().getSelectedItem().toString();
            switch (selectedType) {
                case "Header":
                    container.setVisible(false);                    
                    container.getChildren().clear();
                    container.getChildren().add(headerTextField);
                    container.setVisible(true);
                    this.sizeToScene();
                    headerTextField.setOnAction(h -> {
                        content = headerTextField.getText();
                    });
    //                    for (Node node : vBox.getChildren()) {
    //                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
    //                            vBox.getChildren().remove(node);
    //                        }
    //                    }
    //                    vBox.getChildren().add(headerTextField);
                    break;
                case "Paragraph":
                    container.setVisible(false);
                    container.getChildren().clear();
                    container.getChildren().add(paragraphTextField);
                    container.setVisible(true);
                    this.sizeToScene();
                    paragraphTextField.setOnKeyReleased(p -> {
                        content = headerTextField.getText();
                    });
    //                    for (Node node : vBox.getChildren()) {
    //                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
    //                            vBox.getChildren().remove(node);
    //                        }
    //                    }                    
    //                    vBox.getChildren().add(paragraphTextField);
                    break; 
                case "List":
                    container.getChildren().clear();
                    container.getChildren().add(listAddButton);
                    container.getChildren().add(listTextField);
                    container.setVisible(true);
                    listAddButton.setOnAction(f -> {
                        container.setVisible(false);
                        container.getChildren().add(new TextField());
                        container.setVisible(true);
                        this.sizeToScene();
                    });
                    this.sizeToScene();
                    listTextField.setOnAction(h -> {
                        listContent.add(listTextField.getText());
                    });
                    
    //                    for (Node node : vBox.getChildren()) {
    //                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
    //                            vBox.getChildren().remove(node);
    //                        }
    //                    }
    //                    vBox.getChildren().add(listAddButton);
    //                    vBox.getChildren().add(listTextField);
    //                    listAddButton.setOnAction(f -> {
    //                        vBox.getChildren().add(listTextField);
    //                    });
                    break;
                case "Footer":
                    container.setVisible(false);                    
                    container.getChildren().clear();
                    container.getChildren().add(footerTextField);
                    container.setVisible(true);
                    this.sizeToScene();                    
    //                    for (Node node : vBox.getChildren()) {
    //                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
    //                            vBox.getChildren().remove(node);
    //                        }
    //                    }
    //                    vBox.getChildren().add(headerTextField);
                    break;                    
            }
        });

        
	okButton.setOnAction(e -> {
	    selectedType = textComboBox.getSelectionModel().getSelectedItem().toString();
            this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	textComboBox.getStyleClass().add(CSS_CLASS_BUTN);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Text Component");
	setScene(scene);
    }
    
    public String getSelectedType() {
	return selectedType;
    }
    
    public String getContent() {
	return content;
    }
}

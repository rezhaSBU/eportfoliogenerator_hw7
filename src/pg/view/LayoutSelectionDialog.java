package pg.view;

import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.ICON_LAYOUT1;
import static pg.StartConts.ICON_LAYOUT2;
import static pg.StartConts.ICON_LAYOUT3;
import static pg.StartConts.ICON_LAYOUT4;
import static pg.StartConts.ICON_LAYOUT5;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.PATH_ICONS;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class LayoutSelectionDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label layoutPromptLabel;
    Button okButton;
    Button center;
    Button left;
    Button right;
    Button topleft;
    Button topright;
    String selectedLayout = "center";
    
    public LayoutSelectionDialog() {
	layoutPromptLabel = new Label("Select a layout Scheme");
//layout buttons
        String imagePath = "file:" + PATH_ICONS + ICON_LAYOUT1;
	Image buttonImage = new Image(imagePath);
	center = new Button();
	center.setDisable(false);
	center.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_LAYOUT2;
	buttonImage = new Image(imagePath);
	topleft = new Button();
	topleft.setDisable(false);
	topleft.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_LAYOUT3;
	buttonImage = new Image(imagePath);
	left = new Button();
	left.setDisable(false);
	left.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_LAYOUT4;
	buttonImage = new Image(imagePath);
	right = new Button();
	right.setDisable(false);
	right.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_LAYOUT5;
	buttonImage = new Image(imagePath);
	topright = new Button();
	topright.setDisable(false);
	topright.setGraphic(new ImageView(buttonImage));
// ok button        
	okButton = new Button(OK_BUTTON_TEXT);
//set nodes as children	
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(layoutPromptLabel);
        
	gBox.add(vBox, 1, 0, 3, 1);
        gBox.add(center, 0, 2);
	gBox.add(topleft, 1, 2);
	gBox.add(left, 2, 2);
	gBox.add(right, 3, 2);
	gBox.add(topright, 4, 2);
        gBox.add(okButton, 4, 3);
        GridPane.setHalignment(okButton, HPos.RIGHT);

	center.setOnAction(e -> {
	    selectedLayout = "center";
	});
	topleft.setOnAction(e -> {
	    selectedLayout = "topleft";
	});
	left.setOnAction(e -> {
	    selectedLayout = "left";
	});
	right.setOnAction(e -> {
	    selectedLayout = "right";
	});
	topright.setOnAction(e -> {
	    selectedLayout = "topright";
	});
	okButton.setOnAction(e -> {
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	center.getStyleClass().add(CSS_CLASS_BUTN);
	topleft.getStyleClass().add(CSS_CLASS_BUTN);
	left.getStyleClass().add(CSS_CLASS_BUTN);
	right.getStyleClass().add(CSS_CLASS_BUTN);
	topright.getStyleClass().add(CSS_CLASS_BUTN);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Layout Scheme Selection");
	setScene(scene);
    }
    
    public String getSelectedLayout() {
	return selectedLayout;
    }
}

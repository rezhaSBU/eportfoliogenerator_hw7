package pg.view;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.ENGLISH_LANG;
import static pg.StartConts.SPANISH_LANG;
import static pg.StartConts.LABEL_LANGUAGE_SELECTION_PROMPT;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class SlideshowDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label slideshowPromptLabel;
    Button loadButton;
    Button addButton;
    Button removeButton;
    Button upButton;
    Button downButton;
    Button okButton;
    ImageView imv;
    TextField captionField;
    public SlideshowDialog() {
	slideshowPromptLabel = new Label("Select an slideshow");
	imv = new ImageView();
	okButton = new Button(OK_BUTTON_TEXT);
	loadButton = new Button("Upload");
	addButton = new Button("Add");
	removeButton = new Button("Remove");
	upButton = new Button("Move up");
	downButton = new Button("Move down");
        captionField = new TextField("Caption");
        
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(slideshowPromptLabel);
	vBox.getChildren().add(loadButton);
	vBox.getChildren().add(imv);
	vBox.getChildren().add(addButton);
	vBox.getChildren().add(removeButton);
	vBox.getChildren().add(upButton);
	vBox.getChildren().add(downButton);
	vBox.getChildren().add(captionField);
	gBox.add(vBox, 0, 0, 2, 1);
        gBox.add(okButton, 1, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
        
        loadButton.setOnAction(e -> {
	    FileChooser imageFileChooser = new FileChooser();
             
            FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
            FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
            FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
            imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);

            // LET'S OPEN THE FILE CHOOSER
            File file = imageFileChooser.showOpenDialog(null);
            if (file != null) {
                String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
                String fileName = file.getName(); 
            }
            URL fileURL = null;
            try {
                fileURL = file.toURI().toURL();
            } catch (MalformedURLException ex) {
                Logger.getLogger(SlideshowDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
	    Image pageImage = new Image(fileURL.toExternalForm());
	    imv.setImage(pageImage);
	});
        
	okButton.setOnAction(e -> {
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Slideshow Component");
	setScene(scene);
    }
    
    public String getSelectedSlideshow() {
	return captionField.getText().toString();
    }
}

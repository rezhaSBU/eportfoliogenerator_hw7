package pg.view;

import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.ICON_COLOR1;
import static pg.StartConts.ICON_COLOR2;
import static pg.StartConts.ICON_COLOR3;
import static pg.StartConts.ICON_COLOR4;
import static pg.StartConts.ICON_COLOR5;
import static pg.StartConts.ICON_LAYOUT1;
import static pg.StartConts.ICON_LAYOUT2;
import static pg.StartConts.ICON_LAYOUT3;
import static pg.StartConts.ICON_LAYOUT4;
import static pg.StartConts.ICON_LAYOUT5;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.PATH_ICONS;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class ColorSelectionDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label colorPromptLabel;
    Button okButton;
    Button bubblegum;
    Button sbu;
    Button wildwest;
    Button starrynight;
    Button botany;
    String selectedColor = "bubblegum";
    
    public ColorSelectionDialog() {
	colorPromptLabel = new Label("Select a color Scheme");

//color buttons
	String imagePath = "file:" + PATH_ICONS + ICON_COLOR1;
	Image buttonImage = new Image(imagePath);
	bubblegum = new Button();
	bubblegum.setDisable(false);
	bubblegum.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_COLOR2;
	buttonImage = new Image(imagePath);
	sbu = new Button();
	sbu.setDisable(false);
	sbu.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_COLOR3;
	buttonImage = new Image(imagePath);
	wildwest = new Button();
	wildwest.setDisable(false);
	wildwest.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_COLOR4;
	buttonImage = new Image(imagePath);
	starrynight = new Button();
	starrynight.setDisable(false);
	starrynight.setGraphic(new ImageView(buttonImage));
	imagePath = "file:" + PATH_ICONS + ICON_COLOR5;
	buttonImage = new Image(imagePath);
	botany = new Button();
	botany.setDisable(false);
	botany.setGraphic(new ImageView(buttonImage));
// ok button        
	okButton = new Button(OK_BUTTON_TEXT);
//set nodes as children	
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(colorPromptLabel);

	gBox.add(vBox, 1, 0, 3, 1);
	gBox.add(bubblegum, 0, 2);
	gBox.add(sbu, 1, 2);
	gBox.add(wildwest, 2, 2);
	gBox.add(starrynight, 3, 2);
	gBox.add(botany, 4, 2);
        gBox.add(okButton, 4, 3);
        GridPane.setHalignment(okButton, HPos.RIGHT);

	bubblegum.setOnAction(e -> {
	    selectedColor = "bubblegum";
	});
	sbu.setOnAction(e -> {
	    selectedColor = "sbu";
	});
	wildwest.setOnAction(e -> {
	    selectedColor = "wildwest";
	});
	starrynight.setOnAction(e -> {
	    selectedColor = "starrynight";
	});
	botany.setOnAction(e -> {
	    selectedColor = "botany";
	});
	okButton.setOnAction(e -> {
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	bubblegum.getStyleClass().add(CSS_CLASS_BUTN);
	sbu.getStyleClass().add(CSS_CLASS_BUTN);
	wildwest.getStyleClass().add(CSS_CLASS_BUTN);
	starrynight.getStyleClass().add(CSS_CLASS_BUTN);
	botany.getStyleClass().add(CSS_CLASS_BUTN);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Color Scheme Selection");
	setScene(scene);
    }
    
    public String getSelectedColor() {
	return selectedColor;
    }
}

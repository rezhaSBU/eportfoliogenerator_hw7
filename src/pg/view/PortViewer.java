package pg.view;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import static pg.StartConts.PATH_BASE;
import static pg.StartConts.PATH_PORTS;
import static pg.file.PortFileManager.JSON_EXT;
import static pg.file.PortFileManager.SLASH;
import pg.model.Page;
import pg.model.PortModel;

/**
 * This class provides the UI for the slide show viewer, note that this class is
 * a window and contains all controls inside.
 *
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class PortViewer extends Stage {

    //Directory builders
    boolean siteDir;
    boolean htmlDir;
    boolean cssDir;
    boolean jsDir;
    boolean imgDir;
    
    // THE MAIN UI
    PortGenView parentView;

    // THE DATA FOR THIS PORTFOLIO
    PortModel pages;
    
//    // HERE ARE OUR UI CONTROLS
//    BorderPane borderPane;
//    FlowPane topPane;
//    Label slideShowTitleLabel;
//    ImageView slideShowImageView;
//    VBox bottomPane;
//    Label captionLabel;
//    FlowPane navigationPane;
//    Button previousButton;
//    Button nextButton;

    /**
     * This constructor just initializes the parent and pages references, note
     * that it does not arrange the UI or start the slide show view window.
     *
     * @param initParentView Reference to the main UI.
     */
    public PortViewer(PortGenView initParentView) {
	// KEEP THIS FOR LATER
	parentView = initParentView;

	// GET THE PAGES
	pages = parentView.getPortfolio();
        String title = pages.getTitle();
        //build all the directories
        siteDir = new File("./sites").mkdirs();

        htmlDir = new File("./sites/" + title).mkdirs();
        cssDir = new File("./sites/" + title + SLASH + "css").mkdirs();
        jsDir = new File("./sites/" + title + SLASH + "js").mkdirs();
        //this checks if the image folder exists and clears it out beforehand.
        File chkImgPath = new File("./sites/" + title + SLASH + "img");
        for(File file: chkImgPath.listFiles()) file.delete();
        imgDir = new File("./sites/" + title + SLASH + "img").mkdirs();
        
    }

    /**
     * This method initializes the UI controls and opens the window with the
     * first slide in the portfolio displayed.
     */
    
    public void buildDir(){
        String title = pages.getTitle();
        
        //copy base html file to html directory
        Path source = Paths.get(PATH_BASE + "index.html");
        Path dest = Paths.get("./sites/" + title + SLASH + "index.html");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        //copy base css file to css directory
        source = Paths.get(PATH_BASE + "css//slideStyle.css");
        dest = Paths.get("./sites/" + title + SLASH + "css//slideStyle.css");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        //copy JSON portfolio into js directory
        source = Paths.get(PATH_PORTS + SLASH + title + JSON_EXT);
        dest = Paths.get("./sites/" + title + SLASH + "js/" + title + JSON_EXT);
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        //copy base js file to js directory
        source = Paths.get(PATH_BASE + "js//magic.js");
        dest = Paths.get("./sites/" + title + SLASH + "js//magic.js");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
/////////////////////////////////////
        //copy base js file to js directory
        source = Paths.get(PATH_BASE + "js//cookie.js");
        dest = Paths.get("./sites/" + title + SLASH + "js//cookie.js");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\        

        //get source of each slide image and copy them to img directory
        ObservableList<Page> pageList = pages.getPages();
	for (Page slide : pageList) {
            source = Paths.get(slide.getBannerPath() + slide.getBannerFileName());
            dest = Paths.get("./sites/" + title + SLASH + "img//" + slide.getBannerFileName());
            try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
	}       
    }
    
    public void startPortfolio(){
        String title = pages.getTitle();
        File f = new File("./sites/" + title + SLASH + "index.html");
 
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();

        try {
            webEngine.load(f.toURI().toURL().toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(PortViewer.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scene scene = new Scene(webView, 660, 700);
        setScene(scene);
	this.showAndWait();
    }
    
//    public void startPortfolio() {
//	// FIRST THE TOP PANE
//	topPane = new FlowPane();
//	topPane.setAlignment(Pos.CENTER);
//	slideShowTitleLabel = new Label(pages.getTitle());
//	slideShowTitleLabel.getStyleClass().add(LABEL_PORT_TITLE);
//	topPane.getChildren().add(slideShowTitleLabel);
//
//	// THEN THE CENTER, START WITH THE FIRST IMAGE
//	slideShowImageView = new ImageView();
//	reloadPortfolioImageView();
//
//	// THEN THE BOTTOM PANE
//	bottomPane = new VBox();
//	bottomPane.setAlignment(Pos.CENTER);
//	captionLabel = new Label();
//	if (pages.getPages().size() > 0) {
//	    captionLabel.setText(pages.getSelectedPage().getCaption());
//	}
//	navigationPane = new FlowPane();
//	bottomPane.getChildren().add(captionLabel);
//	bottomPane.getChildren().add(navigationPane);
//
//	// NOW SETUP THE CONTENTS OF THE NAVIGATION PANE
//	navigationPane.setAlignment(Pos.CENTER);
//	previousButton = parentView.initChildButton(navigationPane, ICON_PREVIOUS, LanguagePropertyType.TOOLTIP_PREVIOUS_PAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
//	nextButton = parentView.initChildButton(navigationPane, ICON_NEXT, LanguagePropertyType.TOOLTIP_NEXT_PAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
//
//	// NOW ARRANGE ALL OUR REGIONS
//	borderPane = new BorderPane();
//	borderPane.setTop(topPane);
//	borderPane.setCenter(slideShowImageView);
//	borderPane.setBottom(bottomPane);
//
//	// NOW SETUP THE BUTTON HANDLERS
//	previousButton.setOnAction(e -> {
//	    pages.previous();
//	    reloadPortfolioImageView();
//	    reloadCaption();
//	});
//	nextButton.setOnAction(e -> {
//	    pages.next();
//	    reloadPortfolioImageView();
//	    reloadCaption();
//	});
//
//	// NOW PUT STUFF IN THE STAGE'S SCENE
//	Scene scene = new Scene(borderPane, 1000, 700);
//	setScene(scene);
//	this.showAndWait();
//    }
//
//    // HELPER METHOD
//    private void reloadPortfolioImageView() {
//	try {
//	    Page slide = pages.getSelectedPage();
//	    if (slide == null) {
//		pages.setSelectedPage(pages.getPages().get(0));
//	    }
//	    slide = pages.getSelectedPage();
//	    String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
//	    File file = new File(imagePath);
//	    
//	    // GET AND SET THE IMAGE
//	    URL fileURL = file.toURI().toURL();
//	    Image slideImage = new Image(fileURL.toExternalForm());
//	    slideShowImageView.setImage(slideImage);
//
//	    // AND RESIZE IT
//	    double scaledHeight = DEFAULT_PORT_HEIGHT;
//	    double perc = scaledHeight / slideImage.getHeight();
//	    double scaledWidth = slideImage.getWidth() * perc;
//	    slideShowImageView.setFitWidth(scaledWidth);
//	    slideShowImageView.setFitHeight(scaledHeight);
//	} catch (Exception e) {
//	    // CANNOT SHOW A PORTFOLIO WITHOUT ANY IMAGES
//	    parentView.getErrorHandler().processError(LanguagePropertyType.ERROR_NO_PORTFOLIO_IMAGES);
//	}
//    }
//
//    private void reloadCaption() {
//	Page slide = pages.getSelectedPage();
//	captionLabel.setText(slide.getCaption());
//    }
}
